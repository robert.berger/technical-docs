# containers which make up for a local system

## app-container-redis

app-container scripts (app-container): \
https://gitlab.com/app-container/app-container-redis

image recipe to build the rootfs for the app-container (meta-desire/recipes-core/images): \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-core/images/app-container-image-redis.bb

## app-container-mosquitto

app-container scripts (app-container): \
https://gitlab.com/app-container/app-container-mosquitto

image recipe to build the rootfs for the app-container (meta-desire/recipes-core/images): \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-core/images/app-container-image-mosquitto.bb

## app-container-python3-nmap-srv

app-container script (app-container): \
https://gitlab.com/app-container/app-container-python3-nmap-srv

image recipe to build the rootfs for the app-container (meta-desire/recipes-core/images): \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-core/images/app-container-image-python3-nmap-srv.bb

Recipes for Python repos (meta-desire/recipes-desire/python-iot): \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-nmapwrapper_git.bb \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-tcpclientserver_git.bb

Python repos (recipes-desire/python-iot):

| repo                                                                 | py package            | main | depends on            | reverse dependency |
| -------------------------------------------------------------------- | --------------------- | ---- | --------------------- | ------------------ | 
| https://gitlab.com/recipes-desire/python-iot/python3-nmapwrapper     | nmapwrapper 2.8.3     | yes  | tcpclientserver 1.0.0 | no                 |
| https://gitlab.com/recipes-desire/python-iot/python3-tcpclientserver | tcpclientserver 1.0.0 | no   |                       | nmapwrapper        |

## app-container-python3-data-collector

app-container scripts (app-container): \
https://gitlab.com/app-container/app-container-python3-data-collector

image recipe to build the rootfs for the app-container (meta-desire/recipes-core/images): \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-core/images/app-container-image-python3-data-collector.bb

Recipes for Python repos (meta-desire/recipes-desire/python-iot): \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-localdatacollector_git.bb \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-messageqclient_git.bb \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-tcpclientserver_git.bb \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-mqttbrokerclient_git.bb \
https://gitlab.com/meta-layers/meta-desire/blob/master/recipes-desire/python-iot/python3-mastermind_git.bb

Python repos (recipes-desire/python-iot):

| repo                                                                    | py package               | main | depends on            | reverse dependency |
| ----------------------------------------------------------------------- | ------------------------ | ---- | --------------------- | ------------------ |
| https://gitlab.com/recipes-desire/python-iot/python3-localdatacollector | localdatacollector x.x.x | yes  | messageqclient 1.0.0, tcpclientserver 1.0.0, mqttbrokerclient 2.8.4, mastermind 1.0.0 | no                 |
| https://gitlab.com/recipes-desire/python-iot/python3-messageqclient     | messageqclient           | no   | python3-redis (>=2.10.6), python3-configparser                      | localdatacollector |
| https://gitlab.com/recipes-desire/python-iot/python3-tcpclientserver    | tcpclientserver          | no   |                       | localdatacollector |
| https://gitlab.com/recipes-desire/python-iot/python3-mqttbrokerclient   | mqttbrokerclient         | no   | python3-paho-mqtt (>=1.4.0)    | localdatacollector |
| https://gitlab.com/recipes-desire/python-iot/python3-mastermind         | mastermind               | no   |                       | localdatacollector |

```
$ oe-pkgdata-util list-pkg-files python3-messageqclient
python3-messageqclient:
        /usr/lib/python3.7/site-packages/messageqclient-1.0.0-py3.7.egg-info/PKG-INFO
        /usr/lib/python3.7/site-packages/messageqclient-1.0.0-py3.7.egg-info/SOURCES.txt
        /usr/lib/python3.7/site-packages/messageqclient-1.0.0-py3.7.egg-info/dependency_links.txt
        /usr/lib/python3.7/site-packages/messageqclient-1.0.0-py3.7.egg-info/requires.txt
        /usr/lib/python3.7/site-packages/messageqclient-1.0.0-py3.7.egg-info/top_level.txt
        /usr/lib/python3.7/site-packages/messageqclient-1.0.0-py3.7.egg-info/zip-safe
        /usr/lib/python3.7/site-packages/messageqclient/__init__.py
        /usr/lib/python3.7/site-packages/messageqclient/__main__.py
        /usr/lib/python3.7/site-packages/messageqclient/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/__pycache__/__main__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/__pycache__/constants.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/__pycache__/contact_message_queue.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/constants.py
        /usr/lib/python3.7/site-packages/messageqclient/contact_message_queue.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/__init__.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/handle_list_of_values.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/simple_connection.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/simple_exists_stored_key.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/simple_flush_this.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/simple_set.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/simple_set_key_value.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/__pycache__/simple_test.cpython-37.pyc
        /usr/lib/python3.7/site-packages/messageqclient/redis/handle_list_of_values.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/simple_connection.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/simple_exists_stored_key.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/simple_flush_this.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/simple_set.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/simple_set_key_value.py
        /usr/lib/python3.7/site-packages/messageqclient/redis/simple_test.py
```

```
python3-tcpclientserver:
        /usr/lib/python3.7/site-packages/tcpclientserver-1.0.0-py3.7.egg-info/PKG-INFO
        /usr/lib/python3.7/site-packages/tcpclientserver-1.0.0-py3.7.egg-info/SOURCES.txt
        /usr/lib/python3.7/site-packages/tcpclientserver-1.0.0-py3.7.egg-info/dependency_links.txt
        /usr/lib/python3.7/site-packages/tcpclientserver-1.0.0-py3.7.egg-info/requires.txt
        /usr/lib/python3.7/site-packages/tcpclientserver-1.0.0-py3.7.egg-info/top_level.txt
        /usr/lib/python3.7/site-packages/tcpclientserver-1.0.0-py3.7.egg-info/zip-safe
        /usr/lib/python3.7/site-packages/tcpclientserver/__init__.py
        /usr/lib/python3.7/site-packages/tcpclientserver/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/tcpclientserver/__pycache__/client.cpython-37.pyc
        /usr/lib/python3.7/site-packages/tcpclientserver/__pycache__/server.cpython-37.pyc
        /usr/lib/python3.7/site-packages/tcpclientserver/client.py
        /usr/lib/python3.7/site-packages/tcpclientserver/server.py
```

```
python3-mqttbrokerclient:
        /usr/lib/python3.7/site-packages/mqtt_broker_client-2.8.4-py3.7.egg-info/PKG-INFO
        /usr/lib/python3.7/site-packages/mqtt_broker_client-2.8.4-py3.7.egg-info/SOURCES.txt
        /usr/lib/python3.7/site-packages/mqtt_broker_client-2.8.4-py3.7.egg-info/dependency_links.txt
        /usr/lib/python3.7/site-packages/mqtt_broker_client-2.8.4-py3.7.egg-info/requires.txt
        /usr/lib/python3.7/site-packages/mqtt_broker_client-2.8.4-py3.7.egg-info/top_level.txt
        /usr/lib/python3.7/site-packages/mqtt_broker_client-2.8.4-py3.7.egg-info/zip-safe
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__init__.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/curiosity_rover.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/simple_configuration.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/simple_connection_manager.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/simple_message_manager.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/simple_message_publisher.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/__pycache__/simple_message_subscriber.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/curiosity_rover.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/mosquitto/__init__.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/mosquitto/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/mosquitto/__pycache__/connection.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mqttbrokerclient/mosquitto/connection.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/simple_configuration.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/simple_connection_manager.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/simple_message_manager.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/simple_message_publisher.py
        /usr/lib/python3.7/site-packages/mqttbrokerclient/simple_message_subscriber.py
```

```
python3-mastermind:
        /usr/lib/python3.7/site-packages/master_mind-1.0.0-py3.7.egg-info/PKG-INFO
        /usr/lib/python3.7/site-packages/master_mind-1.0.0-py3.7.egg-info/SOURCES.txt
        /usr/lib/python3.7/site-packages/master_mind-1.0.0-py3.7.egg-info/dependency_links.txt
        /usr/lib/python3.7/site-packages/master_mind-1.0.0-py3.7.egg-info/top_level.txt
        /usr/lib/python3.7/site-packages/master_mind-1.0.0-py3.7.egg-info/zip-safe
        /usr/lib/python3.7/site-packages/mastermind/__init__.py
        /usr/lib/python3.7/site-packages/mastermind/__main__.py
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/__main__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/compare_datasets.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/comparison_rules_engine.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/comparison_rules_names.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/similarity_measures.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/__pycache__/validate_collected_data.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/compare_datasets.py
        /usr/lib/python3.7/site-packages/mastermind/comparison_rules_engine.py
        /usr/lib/python3.7/site-packages/mastermind/comparison_rules_names.py
        /usr/lib/python3.7/site-packages/mastermind/general_rules/__init__.py
        /usr/lib/python3.7/site-packages/mastermind/general_rules/__pycache__/__init__.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/general_rules/__pycache__/generic_rule.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/general_rules/__pycache__/rule_equals_datasetA_datasetB.cpython-37.pyc
        /usr/lib/python3.7/site-packages/mastermind/general_rules/generic_rule.py
        /usr/lib/python3.7/site-packages/mastermind/general_rules/rule_equals_datasetA_datasetB.py
        /usr/lib/python3.7/site-packages/mastermind/similarity_measures.py
        /usr/lib/python3.7/site-packages/mastermind/validate_collected_data.py
```




----------------------------------

ignore below here:

| repo                                                     | py package            | main | depends on            | reverse dependency |
| -------------------------------------------------------- | --------------------- | ---- | --------------------- | ------------------ | 
| https://gitlab.com/mvimplis2013/messageq-client/         | messageqclient x.x.x  | yes  |                       | no                 |



https://gitlab.com/mvimplis2013/messageq-client/tree/master/messageqclient/redis           - package

https://gitlab.com/mvimplis2013/messageq-client/tree/master/messageqclient/vlab/iot        - not needed
https://gitlab.com/mvimplis2013/messageq-client/tree/master/messageqclient/vlab/monitoring - not needed


https://gitlab.com/mvimplis2013/messageq-client/tree/master/messageqclient/zero/heartbeat       

